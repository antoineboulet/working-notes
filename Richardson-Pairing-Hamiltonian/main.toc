\contentsline {title}{Note on the Richardson pairing Hamiltonian}{1}{section*.2}
\contentsline {abstract}{Abstract}{1}{section*.1}
\tocdepth@munge 
\contentsline {section}{\numberline {}Contents}{1}{section*.3}
\tocdepth@restore 
\contentsline {section}{\numberline {I}Exact ground-state energy}{2}{section*.4}
\contentsline {section}{\numberline {II}Hartree-Fock and MBPT solutions}{2}{section*.5}
\contentsline {subsection}{\numberline {A}Hartree-Fock}{2}{section*.6}
\contentsline {subsection}{\numberline {B}Second and third order in MBPT}{2}{section*.7}
\contentsline {section}{\numberline {}References}{3}{section*.8}
