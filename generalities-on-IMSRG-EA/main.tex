\documentclass[reprint,amsmath,amssymb,aps,rmp,showkeys,floatfix]{revtex4-1}
 % superscriptaddress                % useful for collaboration
 % \usepackage{catchfilebetweentags} % useful for collaboration
\input{../header_notes.tex} \input{../bibliography_style.tex} \input{../abreviation.tex}  \input{../math_commands.tex}


\begin{document} \input{FrontBackMatter.tex}


\section{Basic presentation of the Renormalization Group Methods}


\subsection{Similarity Renormalization Group}

The main concept of the Similarity Renormalization Group (\srg) is to simplify the structure of the many-body Hamiltonian $\op{H}$ (view as a matrix) using a unitary transformation of the starting Hamiltonian $\op{H}(0)$ parameterized by a \emph{flow parameter} noted $s$:
\bsubeq
\begin{align}
  \op{H}(s) = \op{U}(s) \op{H}(0)\op{U}\odag(s)
\end{align}
Then, taking the derivative of the transformed Hamiltonian according to the flow parameter, we get the \emph{operator flow equation}:
\begin{align}
  \deriv{}{s} \op{H}(s) = [\op{\eta}(s),\op{H}(s)]
\end{align}
where the anti-commutator is noted $[\op{A},\op{B}] = \op{A}\op{B}-\op{B}\op{A}$ and where the anti-Hermitian operator is defined as:
\begin{align}
  \op{\eta}(s) = \deriv{}{s} \op{U}(s)\op{U}\odag(s) = -  \op{\eta}\odag(s)
\end{align}
Finally, we can choose the flow operator $\op{eta}$ to transform the Hamiltonian in an (almost) arbitrary suitable form:
\esubeq

\subsubsection{Wegner generator}
By splitting the Hamiltonian in a diagonal and off-diagonal part, \ie $\op{H}(s) = \op{H}_{d}(s) + \op{H}_{od}(s)$, the generator defined as:
\begin{align}
  \op{\eta}(s) = [\op{H}_{d}(s) , \op{H}_{od}(s)]
\end{align}
suppress the off diagonal part along the evolution of the flow equation.
Note that \emph{diagonal} means a block or band diagonal structure, \ie a decoupling of momentum or energy scales.

\subsubsection{White generator}


\subsection{In-Medium Similarity Renormalization Group}

The drawback of the \srg method to solve the Schrödinger equation of a many-body systems is the necessity to perform an evolution of a large dimensional matrix and the systematic is inefficient for realistic systems.
To solve the problem, we introduce the in-medium \srg (\imsrg) method.

We consider the many-body Hamiltonian in a basis build from a reference many-body state $\ket{\Phi}$, \eg Slater determinant, and its $n$ multi particle-hole ($ph$) excitations $\ket{\Phi_{nh}^{np}}$.
The goal is here to evolve continuously the Hamiltonian such that the matrix element $\mel{\Phi}{\op{H}(s)}{\Phi}$ becomes decoupled from all excitation in the limit $s \to \infty$: the eigenvalue of $\op{H}$ will be so easily determined and the unitary transform will provide a mapping of the many-body reference state $\ket{\Phi}$ onto an exact many-body state $\ket{\Psi}$ of $\op{H}(0)$ as the ground state (\gs) of the systems.

We introduce the creation and annihilation operator, noted $c\odag_i$ and $c_i$, and the notation\footnote{Here, we change the notation according to \cite{Hergert2018a} to be able to use Einstein sum rules in all this notes.}:
\bsubeq
\begin{align}
  C^{i_1,\cdots i_n}_{j_1,\cdots j_n} \equiv c\odag_{i_1} \cdots c\odag_{i_n}  c_{j_n} \cdots c_{j_1}
\end{align}
and the normal ordered operators:
\begin{align}
  \noo{C_{k}^{i}} &\equiv C_{k}^{i} - \rho_{k}^{i}
  \\
  \noo{C_{kl}^{ij}} &\equiv C_{kl}^{ij}
  \nonumber\\   & - \noo{C_{k}^{i}}\rho_{l}^{j} + \noo{C_{l}^{i}}\rho_{k}^{j} +\noo{C_{k}^{j}}\rho_{l}^{i}  -\noo{C_{l}^{j}}\rho_{k}^{i}
  \nonumber\\   & - \rho_{k}^{i}\rho_{l}^{j} + \rho_{l}^{i}\rho_{k}^{j} +\rho_{k}^{j}\rho_{l}^{i}  -\rho_{l}^{j}\rho_{k}^{i}
\end{align}
\esubeq
and so one, where $\rho_{k}^{i} \equiv \mel{\Phi}{C_{k}^{i}}{\Phi}$ denotes the \emph{one-body density matrix} of the many-body reference state $\ket{\Phi}$,
and more generally $\rho_{k_1,\cdots k_n}^{i_1,\cdots i_n} = \mel{\Phi}{C_{k_1,\cdots k_n}^{i_1,\cdots i_n}}{\Phi}$ stand for the \emph{$n$-body density matrices} of the reference state.

We can show that a $A$-body nuclear Hamiltonian (with two-interactions only) $\op{H} = \op{T} + \op{V}$ take the form:
\bsubeq
\begin{align}
  \op{H} = E &+ {1 \over 1!^2} \sum_{ij} f^i_j \noo{C^j_i} + {1 \over 2!^2} \sum_{ijkl} \Gamma^{ij}_{kl} \noo{C^{kl}_{ij}}
   %+ {1 \over 3!^2} W^{ijk}_{lmn} \noo{A^{lmn}_{ijk}}
\end{align}
where:
\begin{align}
  E &\equiv  t^{a}_{b}\rho_{a}^{b}
     +{1 \over 2!^2}\left[t^{ab}_{cd}+ v^{ab}_{cd}\right]\rho^{cd}_{ab}
     %\nonumber \\    &
     %+ \frac{1}{3!^2}v^{abc}_{def} \rho^{def}_{abc}
  \\
  f^{i}_{j} &\equiv t^{i}_{j} + \left[t^{ia}_{jb} + v^{ia}_{jb}\right]\rho^{b}_{a}
  % + \frac{1}{2!^2}v^{iab}_{jcd}\rho^{cd}_{ab}
  \\
  \Gamma^{ij}_{kl} &\equiv t^{ij}_{kl} + v^{ij}_{kl}
  %+ v^{ija}_{klb}\rho^{b}_{a}
  %\\
  %W^{ijk}_{lmn}&\equiv v^{ijk}_{lmn}
\end{align}
Note that, to recover the result of \cite{Hergert2018a}, we have to make the replacement\footnote{This choice is made here only to simplify the notations and not be confusing by trivial numerical factors.}:
\begin{align*}
  t^{a}_{b} \to {A-1 \over A} t^{a}_{b} \qquad
  t^{ij}_{kl} \to {1 \over A}t^{ij}_{kl}
\end{align*}
\esubeq
Using this representation of the Hamiltonian and the Wick theorem, we can obtain simply the matrix element coupling the the reference may-body state to $np$-$nh$ excitations as:
\bsubeq
\begin{align}
  \langle{\Phi_h^p}|{\noo{\op{H}}}\ket{\Phi} &= \mel{\Phi}{\noo{C_h^p}\noo{\op{H}}}{\Phi} = f_h^p
  \\
  \langle{\Phi_{hh\oprime}^{pp\oprime}}|{\noo{\op{H}}}\ket{\Phi} &= \mel{\Phi}{\noo{C_{hh\oprime}^{pp\oprime}}\noo{\op{H}}}{\Phi} = \Gamma_{hh\oprime}^{pp\oprime}
\end{align}
and obtain the off-diagonal Hamiltonian:
\begin{align}
  \op{H}_{od} \equiv \sum_{ph} f^p_h \noo{C^h_p} + {1 \over 2!^2} \sum_{pp\oprime hh\oprime}\Gamma^{pp\oprime}_{hh\oprime} \noo{C_{pp\oprime}^{hh\oprime}}
\end{align}
\esubeq

\subsubsection{Correlated many-body reference state}

We can generalize the formalism starting from a correlated reference state using a generalized normal ordering reling on the \emph{irreducible $n$-body density matrices}:
\bsubeq
\begin{align}
  \lambda_k^i &= \rho_{k}^{i} = \mel{\Phi}{C_{k}^{i}}{\Phi}
  \\
  \lambda^{ij}_{kl} &= \rho^{ij}_{kl} - \{\lambda_k^i\lambda_l^j\}
%  \\
%  \lambda^{ijk}_{lmn} &= \rho^{ijk}_{lmn} -\mathcal{A}\{\lambda_l^i\lambda_{mn}^{jk}\} -\mathcal{A}\{\lambda_l^i\lambda_m^j\lambda_n^k\}
\end{align}
\esubeq
where $\{\cdots\}$ denote the operator which fully antisymmetrized the indicies of the expression.
Note that the irreducible densities encode the correlation of the reference state.
Finally, the generalized normal ordered operators are given as before using simply the change $\rho \to \lambda$.

\subsubsection{Multireference IMSR}

In practice, to get a computation feasible, we truncate the operator at the two-body level to close the system of flow equations, \ie $\op{H}(s) = E(s) + \op{f}(s) +\op{\Gamma}(s)$.
Using this truncation scheme, the flow equations reads:
\begin{widetext}
\bsubeq
\begin{align}
  \deriv{E}{s}
  &=
  \sum_{ab}(n_{a}-n_{b})\eta^{a}_{b}f^{b}_{a}
  +\frac{1}{4}\sum_{abcd} \left(\eta^{ab}_{cd}\Gamma^{cd}_{ab}-\Gamma^{ab}_{cd}\eta^{cd}_{ab}\right)
                          n_{a}n_{b}\bar{n}_{c}\bar{n}_{d}
  \nonumber \\
  &
  +\frac{1}{4}\sum_{abcd}\left(\deriv{}{s}\Gamma^{ab}_{cd}\right)\lambda_{ab}^{cd}
  +\frac{1}{4}\sum_{abcdklm}\left(\eta^{ab}_{cd}\Gamma^{kl}_{am}-\Gamma^{ab}_{cd}\eta^{kl}_{am}\right)\lambda_{bkl}^{cdm}
  \\
%
  \deriv{}{s}f^{i}_{j} &=
  \sum_{a}\left(\eta^{i}_{a}f^{a}_{j}-f^{i}_{a}\eta^{a}_{j}\right)
  +\sum_{ab}\left(\eta^{a}_{b}\Gamma^{bi}_{aj}-f^{a}_{b}\eta^{bi}_{aj}\right)(n_{a}-n_{b})
  \nonumber \\
  &
  +\frac{1}{2}\sum_{abc} \left(\eta^{ia}_{bc}\Gamma^{bc}_{ja}-
                              \Gamma^{ia}_{bc}\eta^{bc}_{ja}\right)
                         \left(n_{a}\bar{n}_{b}\bar{n}_{c}+\bar{n}_{a}n_{b}n_{c}\right)
  \nonumber \\
  &
  +\frac{1}{4}\sum_{abcde}\left(\eta^{ia}_{bc}\Gamma^{de}_{ja}-\Gamma^{ia}_{bc}\eta^{de}_{ja}\right)\lambda_{de}^{bc}
  +\sum_{abcde}\left(\eta^{ia}_{bc}\Gamma^{be}_{jd}-\Gamma^{ia}_{bc}\eta^{be}_{jd}\right)\lambda_{ae}^{cd}
  \nonumber \\
  &
  -\frac{1}{2}\sum_{abcde}\left(\eta^{ia}_{jb}\Gamma^{cd}_{ae}-\Gamma^{ia}_{jb}\eta^{cd}_{ae}\right)\lambda_{cd}^{be}
  +\frac{1}{2}\sum_{abcde}\left(\eta^{ia}_{jb}\Gamma^{bc}_{de}-\Gamma^{ia}_{jb}\eta^{bc}_{de}\right)\lambda_{ac}^{de}
  \\
%
  \deriv{}{s}\Gamma^{ij}_{kl}&=
  \sum_{a}\left(\eta^{i}_{a}\Gamma^{aj}_{kl}+\eta^{j}_{a}\Gamma^{ia}_{kl}
               -\eta^{a}_{k}\Gamma^{ij}_{al}-\eta^{a}_{l}\Gamma^{ij}_{ka}
               -f^{i}_{a}\eta^{aj}_{kl}-f^{j}_{a}\eta^{ia}_{kl}
               +f^{a}_{k}\eta^{ij}_{al}+f^{a}_{l}\eta^{ij}_{ka} \right)
  \nonumber\\
  &
    +\frac{1}{2}\sum_{ab}\left(\eta^{ij}_{ab}\Gamma^{ab}_{kl}-\Gamma^{ij}_{ab}\eta^{ab}_{kl}\right)
     \left(1-n_{a}-n_{b}\right)
    +\sum_{ab}(n_{a}-n_{b})\left[\left(\eta^{ia}_{kb}\Gamma^{jb}_{la}-\Gamma^{ia}_{kb}\eta^{jb}_{la}\right)-\left(\eta^{ja}_{kb}\Gamma^{ib}_{la}-\Gamma^{ja}_{kb}\eta^{ib}_{la}\right)\right]
\end{align}
\esubeq
\end{widetext}
where the $s$-dependence is implicit to shorthand the equations.
These equations have been simplified using the fact that we work in the natural orbital basis, \ie $\lambda_i^j = n_i \delta_i^j$ where $n_k$ is the occupation number of the $k^\mathrm{th}$ orbital in the correlated reference state, and similarly,  $\bar{n}_i = 1-n_i$ are the eigenvalues of the hole density matrix $\xi_i^j \equiv \lambda_i^j -\delta_i^j$.










\section{Path integral effective action formalism for DFT}


\subsection{Generalities}

We start by defining the generating partition function:
\begin{align}
  Z[J] = \e^{\displaystyle  -W[J]} = \Tr[\e^{\displaystyle -\beta[\op{H}+J(X)\cdot \op{Q}(X) ]}]
\end{align}
where $\op{H}$ is the many-body Hamiltonian, $J$ is a source coupled to the operator $\op{Q}$, and $\beta$ can be seen (in imaginary-time formalism, \ie after Wick rotation) as the inverse temperature.
The notation $J\cdot \op{Q}$ refer to all necessary sumation/intergation, \eg if $\op{Q} = \op{\rho}$ is the one-body density, this notation means:
\begin{align}
  J(X)\cdot \op{\rho}(X) \equiv \fint_2  J(x_1|x_2) \op{\rho}(x_2|x_1)
\end{align}
where $x = \{\bm{r}\sigma\tau\}$ ($\bm{r}$ is the spacial coordinate, $t$ is the imaginary-time coordinate,
and $\sigma = \uparrow, \downarrow$ [$\tau = p, n$] is the [iso]spin projection on the quantization axis), and where
we have use the shorthand notation defined as:
\begin{align*}
  \fint_n   \equiv   \sum_{\sigma_i=\sigma_1}^{\sigma_n}\sum_{\tau_i=\tau_1}^{\tau_n} \left[\prod_{i=1}^n \int \d \bm{r_i} \right]
\end{align*}

The \emph{thermal} expectation value of $\op{Q}$ defined as:
\begin{align}
  Q &= \langle \op{Q} \rangle_J(Y)
  \nonumber \\ &= {1\over Z[J]} \Tr[\op{Q}(Y) \,\e^{\displaystyle -\beta[\op{H}+J(X)\cdot \op{Q}(X) ]}]
  \nonumber \\ &= - {1 \over \beta} {1\over Z[J]} \fderiv{Z[J]}{J(Y)} = {1 \over \beta}  \fderiv{W[J]}{J(Y)}
\end{align}
This formalism can be generalize by introducing more coupling source, \eg pairing or kinetic density \cite{CHbook:EFTforDFT}.
So we will define a set of source denoted $\mathcal{J} \equiv \{J_n\}$ coupled to a set of operator $\op{\mathcal{Q}} \equiv \{\op{Q}_n\}$.
We can prove (see appendix XX) that the functional $W[J]$ which give a description of the systems in terms of the source $J$ is strictly concave, meaning that we can always inverse the last relationships to find $J[Q]$.
The goal is to get a description in terms of $Q$. This can be done using a Legendre transformation leading to the effective action:
\begin{align}
  \Gamma[Q] = W[J[Q]] - \beta J(X)\cdot Q(X)
\end{align}
which is strictly convex (see appendix XX).
Thus, taking the functional derivative of the effective action according to $Q$, we obtain:
\begin{align}
  \fderiv{\Gamma[Q]}{Q(X)} = -\beta J(X)
\end{align}
The physical system is recover in the vanishing source limit, \ie $J =0$, hence the variation principle arrising and saying that the effective action is minimum at the exact expectation value of $\op{Q}$:
\begin{align}
  \left. \fderiv{\Gamma[Q]}{Q(X)} \right|_{Q\,=\,Q_0}= 0
\end{align}

In the \emph{zero-temperature limit} ($\beta \to \infty$), $Q_0 = Q_\gs$ is the exact ground state (\gs) expectation value.
Consequently, from this solution, we can determine the \gs energy of the system as\footnote{Assuming that the \gs is isolated from the rest.}:
\begin{align}
  E_\gs = \lim_{\beta \to \infty} {1 \over \beta} {\Gamma}[\rho_\gs]
\end{align}



\subsection{Path integral formalism}

We start by defining the fermionic \emph{field operators} in term of the creation and annihilation operators as \cite{book:FetterWalecka}:
\bsubeq
\begin{align}
  {\psi}({y}) &= \sum_k \phi_k({y})c_k
  \\
  {\psi}\odag({y}) &= \sum_k \phi_k^*({y})c_k\odag
\end{align}
\esubeq
where $\phi$ are the single-particle wave functions, $k$ denote a complete set of single-
particle quantum numbers, and $y = \{\bm{r}t\sigma\tau\}$ ($\bm{r}$ is the spacial coordinate, $t$ is the imaginary-time coordinate,
and $\sigma = \uparrow, \downarrow$ [$\tau = p, n$] is the [iso]spin projection on the quantization axis).
The many-body Hamiltonian $\op{H} = \op{T} + \op{V} + \op{U}$, where we have add an external background potential $\op{U}$ (in nuclei, $\op{U} = 0$), can be rewritten in terms of these fields as:
\begin{align}
  \op{H} &= \fint_2 {\psi}\odag({y_1}) [\mathcal{T}({x_1}|x_2) + \mathcal{U}({x_1}|x_2)] {\psi}({y_2})
  \nonumber \\ &+ {1\over2} \fint_4
  {\psi}\odag({y_1}){\psi}\odag({y_2})\mathcal{V}(x_1,x_2|x_3,x_4)  {\psi}({y_4}){\psi}({y_3})
\end{align}
where we have use the shorthand notation defined as:
\begin{align*}
  \fint_n   \equiv   \sum_{\sigma_i=\sigma_1}^{\sigma_n}\sum_{\tau_i=\tau_1}^{\tau_n} \left[\prod_{i=1}^n \int \d \bm{r_i} \right]
\end{align*}
\iffalse
\bsubeq
As before, we can eventually introducing the normal ordering for the field operators:
\begin{align}
  \mathcal{C}(x_{j_1}\cdots x_{j_n}|x_{i_1}\cdots x_{i_n}) \equiv& \,\psi\odag(x_{i_1}) \cdots \psi\odag(x_{i_n})
  \nonumber \\ & \,\psi(x_{j_n}) \cdots \psi(x_{j_1})
\end{align}
defined as:
\begin{align}
  \noo{\mathcal{C}(x_{k}|x_{i})} &\equiv \mathcal{C}(x_{k}|x_{i}) - \rho(x_{k}|x_{i})
\end{align}
and so one, and then we can express this $A$-body nuclear Hamiltonian as a zero-, one-, and two-body contributions:
\begin{align}
  \op{H} &= E + \op{f} + \op{\Gamma}
\end{align}
\esubeq
\fi


Using imaginary-time formalism, the (Euclidean) non-relativistic action of the systems is given by:
\begin{align}
  S_0[\psi\odag,\psi] = \int_0^\beta \d t  \left\{ \fint_1  {\psi}\odag(y) [\partial_t-\mu] {\psi}(y) + \op{H}  \right\} %- \mu \op{N}
\end{align}
where the number of particle is fixed by the Lagrange multiplier $\mu$ (chemical potential) coupled to the total number operator.
In this field operator formalism, the one-body density operator take the form:
\begin{align}
 \op{\rho}({y},{y\oprime}) = \op{\psi}\odag({y\oprime})  \op{\psi}({y})
\end{align}


To obtain the functional of the density, we introduce a source field $J$ coupled to the one-body density matrix and the action becomes:
\begin{align}
  S[\psi\odag,\psi,J] = S_0[\psi\odag,\psi] + J \cdot \op{Q}
\end{align}
Note that for the shack of generality, we keep the notation $\op{Q}$ to design the operator coupled to the auxiliary source $J$.
Then the partition function, which encode all the physical information of the many-body systems, is defined as:
\begin{align}
  Z[J] &\equiv \e^{-W[J]} = \Tr[\e^{\displaystyle -S[\psi\odag,\psi,J] }]
  \nonumber \\ &= \int \mathcal{D}\psi\odag \mathcal{D}\psi \, \e^{\displaystyle -S[\psi\odag,\psi,J]}
\end{align}
where we have rewrite the trace as a path integral over the Grassmann field $\psi\odag,\psi$ \cite{book:NegeleOrland}.


\iffalse
The one-body density matrix in presence of the source is then given by the functional derivative:
\begin{align}
  {\rho}(x_2|x_1) \equiv \langle \op{\rho}(x_2|x_1) \rangle_J = \fderiv{W[J]}{J(x_1|x_2)}
   = \fderiv{W[J]}{J(x_1|x_2)}
\end{align}



We suppose the inversion to find $J[\rho]$ possible, and we make the Legendre transform of $W$ with respect to the source $J$ to define the effective action\footnote{Sometimes defined as ${\Gamma}[\rho] = \sup_J \left\{ W[J] + \beta  J \cdot {\rho} \right\}$ [to be clarify].}:
\begin{align}
  {\Gamma}[\rho] =  W[J[\rho]] - J[\rho] \cdot {\rho}
\end{align}
We can show that this effective action verify the properties:
\begin{align}
  \fderiv{{\Gamma}[\rho]}{J(x_1|x_2)} &= 0
  \\
  \fderiv{{\Gamma}[\rho]}{{\rho}(x_2|x_1)} &= -J(x_1|x_2)
\end{align}
In particular, the \gs of the systems correspond to the vanishing external source, \ie $J = 0$.
Thus, the last equation determine the (exact) \gs one-body density of the systems:
\begin{align}
  \fderiv{{\Gamma}[\rho]}{{\rho}(x_2|x_1)}\Bigg|_{\rho_{\gs}} &= 0 \quad \to \quad \rho_{\gs}(x_2|x_1)
\end{align}
From this solution, we can determine the \gs energy of the system as:
\begin{align}
  E_\gs = \lim_{\beta \to \infty} {1 \over \beta} {\Gamma}[\rho_\gs]
\end{align}
assuming that the \gs is isolated from the rest.
\fi




\subsection{Illustration: development based on variational perturbation theory \cite{Kleinert2011a}}


We start by consider a fundamental (for simplicity, we consider only identical fermions of spin $1/2$) local many-body Hamiltonian:
\begin{align}
  \op{H} &= -{1 \over 2m}\sum_\sigma \int \d\bm{r} \, {\psi}_\sigma\odag(x) \nabla^2  {\psi}_\sigma(x)
  \nonumber \\ &+ {g\over2} \sum_{\sigma\sigma\oprime} \int \d\bm{r} \,
  {\psi}_\sigma\odag(x){\psi}_{\sigma\oprime}\odag(x)  {\psi}_{\sigma\oprime}(x){\psi}_\sigma(x)
\end{align}
where $x = \{\bm{r},t\}$, and $g$ is the strength of the two-body interaction.
The action of the systems in imaginary time formalism is given by:
\begin{align}
  S_0[\psi\odag,\psi] &= \fint_x  {\psi}_\sigma\odag(x) G_{\sigma\sigma\oprime}\mo {\psi}_{\sigma\oprime}(x)
  \nonumber \\ &+  {g\over2} \fint_x
  {\psi}_\sigma\odag(x){\psi}_{\sigma\oprime}\odag(x)  {\psi}_{\sigma\oprime}(x){\psi}_\sigma(x)
\end{align}
where we have defined:
\begin{align}
  \fint_x \equiv \sum_{\sigma\sigma\oprime} \int_x \equiv \sum_{\sigma\sigma\oprime} \int\d t \int \d\bm{r}
\end{align}
and $G_{\sigma\sigma\oprime}\mo \equiv [\partial_t - \xi_\sigma]\delta_{\sigma\sigma\oprime}$ whit $\xi_\sigma = - \nabla^2/2m-\mu_\sigma$ with $\mu_\sigma$ denoting the chemical protential of the particle with spin projection $\sigma$.

Then we introduce the two (complex) auxiliary fields, or classical collective fields, $\kappa,\rho$ coupled in quadratic form to the field operator such as the action rest unchanged but is decomposed in to part and becomes:
\begin{align}
  S_0 \equiv S_0\oprime + S
  &= S_0\oprime - {1\over2} \fint_x f\odag_\sigma \mathcal{A}_{\sigma\sigma\oprime} f_{\sigma\oprime}
  \nonumber \\ &+ {g\over2}  \fint_x
  {\psi}_\sigma\odag(x){\psi}_{\sigma\oprime}\odag(x)  {\psi}_{\sigma\oprime}(x){\psi}_\sigma(x)
\end{align}
where we have introduced the doublet (Nambu spinor) [see appendix XX for more details] $f_\sigma\odag \equiv (\psi\odag_\sigma,\psi_\sigma)$ and the auxiliary matrix:
\begin{align}
  \mathcal{A}_{\sigma\sigma\oprime} =
  \begin{bmatrix}
    \rho_{\sigma\sigma\oprime} &  \kappa_{\sigma\sigma\oprime} \\ \kappa_{\sigma\sigma\oprime}^* & 1-\rho_{\sigma\sigma\oprime}
  \end{bmatrix}
\end{align}
The new \emph{free} action is defined as:
\begin{align}
  S_0\oprime &= \fint_x  {\psi}_\sigma\odag(x) G_0\mo \delta_{\sigma\sigma\oprime}{\psi}_{\sigma\oprime}(x) + {1\over2} \fint_x f\odag_\sigma \mathcal{A}_{\sigma\sigma\oprime} f_{\sigma\oprime}
  \nonumber \\ &\equiv {1\over2} \fint_x f\odag_\sigma \mathcal{A}_{\sigma\sigma\oprime}\oprime f_{\sigma\oprime}
\end{align}
where:
\begin{align}
  \mathcal{A}\oprime_{\sigma\sigma\oprime} =
  \begin{bmatrix}
    G\mo_{\sigma\sigma\oprime}+\rho_{\sigma\sigma\oprime} &  \kappa_{\sigma\sigma\oprime} \\ \kappa_{\sigma\sigma\oprime}^* & -G^{*-1}_{\sigma\sigma\oprime}-\rho^*_{\sigma\sigma\oprime}
  \end{bmatrix}
\end{align}
where $G^{*-1}_{\sigma\sigma\oprime} \equiv [-\partial_t - \xi_\sigma]\delta_{\sigma\sigma\oprime}$.
We can introduce the \emph{free} propagator $G^\kappa$ and $G^\rho$ solution of the matrix equation:
\begin{align}
  \mathcal{A}_{\sigma\sigma\oprime}(x,x\oprime) \cdot
  \begin{bmatrix}
  G^\rho_{\sigma\oprime\sigma^{\prime\prime}}  & G^{\kappa*}_{\sigma\oprime\sigma^{\prime\prime}} \\ G^\kappa_{\sigma\oprime\sigma^{\prime\prime}} & G^\rho_{\sigma\oprime\sigma^{\prime\prime}}
  \end{bmatrix}(x\oprime,x^{\prime\prime})
  \equiv \delta(x-x^{\prime\prime})\delta_{\sigma \sigma^{\prime\prime}}
\end{align}



We define then the generating partition function:
\begin{align}
  Z_0 = \Tr[\e^{\displaystyle -S_0}] =
  \Tr[\e^{\displaystyle -S_0\oprime}] \times \sum_{n=0}^\infty {(-1)^n \over n!}\Tr[S^n]
\end{align}
Then, defining the \emph{free} partition function:
\begin{align}
  Z_0\oprime \equiv \e^{\displaystyle -W_0\oprime} = \Tr[\e^{\displaystyle -S_0\oprime}]
\end{align}
we can show that:
\begin{align}
  \fderiv{W_0\oprime}{\kappa_{\sigma\sigma\oprime}(x,x\oprime)} &= G^\kappa_{\sigma\sigma\oprime}(x,x\oprime)
  \\
  \fderiv{W_0\oprime}{\rho_{\sigma\sigma\oprime}(x,x\oprime)} &= G^\rho_{\sigma\sigma\oprime}(x,x\oprime)
\end{align}


Then, we will evaluate $\Tr[S] = \langle S \rangle$.
Using the Wick theorem, we can write:
\begin{align}
    \langle \psi\odag_\uparrow \psi\odag_\downarrow \psi_\downarrow \psi_\uparrow\rangle
  &= \langle \psi\odag_\uparrow \psi_\uparrow \rangle \langle \psi\odag_\downarrow \psi_\uparrow\rangle
  - \langle \psi\odag_\uparrow \psi_\downarrow \rangle \langle \psi\odag_\downarrow \psi_\uparrow\rangle
  \nonumber \\
  &
  + \langle \psi\odag_\uparrow \psi\odag_\downarrow \rangle \langle \psi_\downarrow \psi\odag_\uparrow\rangle
\end{align}
Introducing the expectation value appearing above as:
\begin{align}
  \bar{\rho}_{\sigma\sigma\oprime} = g \langle \psi\odag_\sigma \psi_{\sigma\oprime} \rangle
  \\
  \bar{\kappa}_{\sigma\sigma\oprime} = g \langle \psi_\sigma \psi_{\sigma\oprime} \rangle
\end{align}
we get:
\begin{align}
  \langle S \rangle &= {1 \over g} \int_x [\bar{\rho}_{\uparrow\uparrow}\bar{\rho}_{\downarrow\downarrow}-\bar{\rho}_{\uparrow\downarrow}\bar{\rho}_{\downarrow\uparrow} + \bar{\kappa}^*_{\downarrow\uparrow}\bar{\kappa}_{\downarrow\uparrow}]
  \nonumber \\
  &- {1 \over 2g} \fint_x [2\bar{\rho}_{\sigma\sigma\oprime}\rho_{\sigma\sigma\oprime}
  +\bar{\kappa}_{\sigma\oprime\sigma}{\kappa}^*_{\sigma\oprime\sigma}
  +\bar{\kappa}^*_{\sigma\sigma\oprime}{\kappa}_{\sigma\oprime\sigma}]
  \\
  &= {1 \over g} \int_x [\bar{\rho}^2+\bar{\kappa}^2 - (2\bar{\rho}\rho+\bar{\kappa}\kappa^*+\bar{\kappa}^*\kappa)]
\end{align}
where the last equality was obtained assuming symmetric system in spin and a local form of the pairing field.


\cleardoublepage
\appendix

\iffalse
\subsection{Kinetic energy as a functional of the one-body density}

We first introduce the free propagator $G_0$ in the vanishing two-body interaction limits, \ie $\op{H} \to \op{H}_0 = \op{T}+\op{U}$,
such that the action reads:
\begin{align}
    S_0[\psi\odag,\psi] = \int_0^\beta \d t  \fint_2  \op{\psi}\odag(x_1) G_0\mo(x_1|x_2) \op{\psi}(x_2)
\end{align}




\subsection{Decomposition of the two-body interaction}
%Todo: generic local int. with central, spin-orbit, tensor part in ST channel.

Considering a generic local two-body interaction decomposed into a central ($I=C$), tensor ($I=T$), and spin-orbit ($I=LS$) part.
The locality of the interaction allow to write \cite{Gebremariam2010a}:
\begin{equation}
  \mel{\bm{r_1}\bm{r_2}}{\mathcal{V}^{\,a\otimes b}_I}{\bm{r_3}\bm{r_4}} = \mathcal{V}^{\,a\otimes b}_I \delta(\bm{r_1}-\bm{r_3})\delta(\bm{r_2}-\bm{r_4})
\end{equation}
where the subscript $a\otimes b$ refers to the spin-isospin components of the particles $a$ and $b$.

Then the different parts of the local interaction are given in terms of the relative and center-of-mass coordinates:
\[
  \bm{r} \equiv \bm{r_1} - \bm{r_2}
  \qquad
  \bm{R} \equiv {1\over2}(\bm{r_1} + \bm{r_2})
\]
We define also the associated momentum operators:
\[
  \bm{k} = {1\over2\i}(\nabla_1 - \nabla_2)
  \qquad
  \i\bm{K} = \nabla_1 + \nabla_2
\]
where $\nabla_i$ act on $\bm{r_i}$.

\bsubeq
\begin{itemize}
  \item \emph{central part}
  \begin{align}
    \mathcal{V}^{\,a\otimes b}_{C}   &= v_C^\beta(r) Q_{\beta}^{\,a\otimes b} O_C^{\,a\otimes b}
    \\
    O_C^{\,a\otimes b} &= 1
  \end{align}
  %
  \item \emph{tensor part}
  \begin{align}
    \mathcal{V}^{\,a\otimes b}_{T}   &= v_T^\beta(r) Q_{\beta}^{\,a\otimes b} O_T^{\,a\otimes b}
    \\
    O_T^{\,a\otimes b} &= 3(\bm{\sigma_a}\cdot\bm{\hat{r}})(\bm{\sigma_b}\cdot\bm{\hat{r}})-\bm{\sigma_a}\cdot\bm{\sigma_b}
  \end{align}
  Generally, we have $v_T^\sigma = v_T^{\sigma\tau} = 0$.
  Actually, the tensor force acts only on triplet spin states and consequently the exchange spin operator can be replaced by $P_\sigma \to 1$ \cite{Negele1972a,Gebremariam2010a} or equivalently $O_{T}^{\,a\otimes b}(\bm{\sigma_a}\cdot\bm{\sigma_b}) \to O_{T}^{\,a\otimes b}$.
  %
  \item \emph{spin-orbit part}
  \begin{align}
    \mathcal{V}^{\,a\otimes b}_{LS} &= v_{LS}^\beta(r)  Q_{\beta}^{\,a\otimes b} O_{LS}^{\,a\otimes b}
    \\
    O_{LS}^{\,a\otimes b} &= -{\i\over2}\bm{r} \times (\nabla-\nabla\oprime)  \cdot (\bm{\sigma_a}+\bm{\sigma_b})
    \nonumber\\ &= \bm{r} \times \bm{k}  \cdot (\bm{\sigma_a}+\bm{\sigma_b})
  \end{align}
  Generally, we have $v_{LS}^\sigma = v_{LS}^{\sigma\tau} = 0$.
  Actually, the spin-orbit force acts only on triplet spin states and consequently the exchange spin operator can be replaced by $P_\sigma \to 1$ \cite{Negele1972a,Gebremariam2010a} or equivalently $O_{LS}^{\,a\otimes b}(\bm{\sigma_a}\cdot\bm{\sigma_b}) \to O_{LS}^{\,a\otimes b}$.
\end{itemize}
\esubeq
In the equations above, we have define the operators $Q_{\beta}^{\,a\otimes b}$ as:
\bsubeq
\begin{align}
    Q_0^{\,a\otimes b} &= 1\\
    Q_\sigma^{\,a\otimes b} &= \bm{\sigma_a}\cdot\bm{\sigma_b}\\
    Q_\tau^{\,a\otimes b} &= \bm{\tau_a}\cdot\bm{\tau_b}\\
    Q_{\sigma\tau}^{\,a\otimes b} &= (\bm{\sigma_a}\cdot\bm{\sigma_b})(\bm{\tau_a}\cdot\bm{\tau_b})
\end{align}
\esubeq





\iffalse
\section{Simplification}

In order to combine the both approach presented briefly above, the \imsrg methods and the effective action formalism, we first simplify the two-body interaction to consider as well as the spin-isospin coupling.
Thus, we consider a local and central two-body interaction independent of the spin-isospin channel, \ie:
\begin{align}
  \mathcal{V}(x_1,x_2|x_3,x_4) = v(r) \delta(x_1-x_3)\delta(x_2-x_4)
\end{align}
where we have introduce the relative and center-of-mass coordinates:
\begin{align*}
  \bm{r} \equiv \bm{r_1}-\bm{r_2}    \qquad \bm{R} \equiv {1 \over 2}(\bm{r_1}+\bm{r_2})
\end{align*}
In the following, the spin-isospin degeneracy will be noted $g$.
By the same way, the kinetic interaction reduce to:
\begin{align}
  \mathcal{T}({x_1}|x_2) = -\delta(x_1-x_2){\nabla^2 \over 2m}
\end{align}
where $m$ denote the mass of the particles.

Thus the Hamiltonian we consider reduce to:
\begin{align}
  \op{H} &= -{1 \over 2m} \sum_{\sigma}\int \d\bm{r_1} \,\op{\psi}_{\sigma}\odag(\bm{r_1}) \nabla^2 \op{\psi}_{\sigma}(\bm{r_1})
  \nonumber \\ &+ {1\over2} \sum_{\sigma\sigma\oprime} \iint  \d\bm{r_1}\, \d\bm{r_2} \, v(r)
  \op{\psi}\odag_{\sigma}(\bm{r_1})\op{\psi}\odag_{\sigma\oprime}(\bm{r_2})   \op{\psi}_{\sigma\oprime}(\bm{r_2})\op{\psi}_{\sigma}(\bm{r_1})
\end{align}
where we simplify here by considering only fermions with spin and without isospin, and introduce the notation $\op{\psi}_{\sigma}$ for the field operators.
\fi



\fi
\newpage
\input{BackEndMatter.tex} \end{document}
