\contentsline {title}{Generalities on In-Medium Similarity Renormalization Group \\ in an effective action formalism}{1}{section*.2}
\contentsline {abstract}{Abstract}{1}{section*.1}
\tocdepth@munge 
\contentsline {section}{\numberline {}Contents}{1}{section*.3}
\tocdepth@restore 
\contentsline {section}{\numberline {I}Basic presentation of the Renormalization Group Methods}{1}{section*.4}
\contentsline {subsection}{\numberline {A}Similarity Renormalization Group}{1}{section*.5}
\contentsline {subsubsection}{\numberline {1}Wegner generator}{1}{section*.6}
\contentsline {subsubsection}{\numberline {2}White generator}{1}{section*.7}
\contentsline {subsection}{\numberline {B}In-Medium Similarity Renormalization Group}{1}{section*.8}
\contentsline {subsubsection}{\numberline {1}Correlated many-body reference state}{2}{section*.9}
\contentsline {subsubsection}{\numberline {2}Multireference IMSR}{2}{section*.10}
\contentsline {section}{\numberline {II}Path integral effective action formalism for DFT}{3}{section*.11}
\contentsline {subsection}{\numberline {A}Generalities}{3}{section*.12}
\contentsline {subsection}{\numberline {B}Path integral formalism}{3}{section*.13}
\contentsline {subsection}{\numberline {C}Illustration: development based on variational perturbation theory \citep {Kleinert2011a}}{4}{section*.14}
\appendix 
\contentsline {section}{\numberline {}References}{6}{section*.15}
