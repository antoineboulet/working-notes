\contentsline {title}{MBPT constrained and BMF DFT: \nobreakspace {}\nobreakspace {}\nobreakspace {}\nobreakspace {}\nobreakspace {}\nobreakspace {}\nobreakspace {}\nobreakspace {}\nobreakspace {}\nobreakspace {}\nobreakspace {}\nobreakspace {}\nobreakspace {}\nobreakspace {}\nobreakspace {}\nobreakspace {}\nobreakspace {}\nobreakspace {}\nobreakspace {}\nobreakspace {}\nobreakspace {}\nobreakspace {}\nobreakspace {}\nobreakspace {}\nobreakspace {}\nobreakspace {}\nobreakspace {}\nobreakspace {}\nobreakspace {}\nobreakspace {}\nobreakspace {}\nobreakspace {}\nobreakspace {}\nobreakspace {}\nobreakspace {}\nobreakspace {}\nobreakspace {}\nobreakspace {}\nobreakspace {}\nobreakspace {}\nobreakspace {}\nobreakspace {}\nobreakspace {}\nobreakspace {}\nobreakspace {}\nobreakspace {}\nobreakspace {}\nobreakspace {}\nobreakspace {}\nobreakspace {}\nobreakspace {}\nobreakspace {}\nobreakspace {} a step towards the EFT formalism of EDF}{1}{section*.2}
\contentsline {abstract}{Abstract}{1}{section*.1}
\tocdepth@munge 
\contentsline {section}{\numberline {}Contents}{1}{section*.3}
\tocdepth@restore 
\contentsline {section}{\numberline {I}Introduction}{2}{section*.4}
\contentsline {section}{\numberline {II}Presentation of the problem}{2}{section*.5}
\contentsline {subsection}{\numberline {A}Hartree-Fock or MBPT at first order}{3}{section*.6}
\contentsline {subsubsection}{\numberline {1}In-Medium Correlated approximation in low- and high-scale}{3}{section*.7}
\contentsline {subsubsection}{\numberline {2}In-Medium Correlated approximation with effective Hamiltonian}{3}{section*.8}
\contentsline {subsection}{\numberline {B}Direct application to ultracold atoms systems I }{3}{section*.9}
\contentsline {subsection}{\numberline {C}Treatment of the pole at HF level}{4}{section*.10}
\contentsline {section}{\numberline {III}Extension of the MBPT with correlated effective interaction }{4}{section*.11}
\contentsline {subsection}{\numberline {A}Second order in MBPT}{4}{section*.12}
\contentsline {subsection}{\numberline {B}IMCA for the second order in MBPT}{4}{section*.13}
\contentsline {subsubsection}{\numberline {1}Low-scale limit}{4}{section*.14}
\contentsline {subsubsection}{\numberline {2}High scale limit}{4}{section*.15}
\contentsline {subsubsection}{\numberline {3}Effective Hamiltonian for second order in MBPT and associated ground state energy at NLO}{5}{section*.16}
\contentsline {subsection}{\numberline {C}Direct application to ultracold atoms systems II }{5}{section*.17}
\contentsline {section}{\numberline {IV}EDF as an EFT? }{5}{section*.18}
\contentsline {subsection}{\numberline {A}Estimation of the error}{7}{section*.19}
\contentsline {subsection}{\numberline {B}Remove the imaginary part }{7}{section*.20}
\contentsline {section}{\numberline {V}Link with IMSRG}{8}{section*.21}
\contentsline {subsection}{\numberline {A}Generate the effective Hamiltonian by unit transform?}{8}{section*.22}
\contentsline {subsubsection}{\numberline {1}Using the full IMCA}{8}{section*.23}
\contentsline {subsubsection}{\numberline {2}Using the restricted IMCA}{9}{section*.24}
\contentsline {subsection}{\numberline {B}Introducing the flow parameter formalism?}{9}{section*.25}
\contentsline {subsection}{\numberline {C}Effective many-body state}{9}{section*.26}
\contentsline {section}{\numberline {VI}Convergence of the method: discussion on the third order extended MBPT with constraints }{9}{section*.27}
\contentsline {subsection}{\numberline {A}IMCA of the loop opperator}{10}{section*.28}
\contentsline {subsection}{\numberline {B}Simplified method in the high-scale IMCA}{10}{section*.29}
\contentsline {section}{\numberline {VII}Mani-body wave functions}{10}{section*.30}
\contentsline {section}{\numberline {VIII}Application to the Richardson pairing Hamiltonian I }{12}{section*.31}
\contentsline {subsection}{\numberline {A}Exact ground-state energy}{12}{section*.32}
\contentsline {subsection}{\numberline {B}Hartree-Fock and MBPT solutions}{12}{section*.33}
\contentsline {subsubsection}{\numberline {1}Hartree-Fock}{12}{section*.34}
\contentsline {subsubsection}{\numberline {2}MBPT results}{13}{section*.35}
\contentsline {subsection}{\numberline {C}MBPT using high-scale limit constraints}{13}{section*.36}
\contentsline {section}{\numberline {IX}Generalization of the limiting high-scale constraint}{15}{section*.37}
\contentsline {subsection}{\numberline {A}Results with finite constraints}{15}{section*.38}
\contentsline {subsubsection}{\numberline {1}Multi-constrained IMCA at HF level}{15}{section*.39}
\contentsline {subsubsection}{\numberline {2}Effective interaction at higher orders}{15}{section*.40}
\contentsline {section}{\numberline {X}Application to the asymmetric nuclear matter equation of state }{20}{section*.41}
\contentsline {subsection}{\numberline {A}Application to the asymmetric nuclear matter equation of state at Hartree-Fock level}{20}{section*.42}
\contentsline {section}{\numberline {XI}Application to the Richardson pairing Hamiltonian II}{20}{section*.43}
\contentsline {section}{\numberline {XII}Application to density dependent Skyrme parameters}{20}{section*.44}
\contentsline {section}{\numberline {XIII}Application to single-particle potential of ultracold atoms}{21}{section*.45}
\contentsline {section}{\numberline {XIV}Extension of the method to general systems }{22}{section*.46}
\contentsline {subsection}{\numberline {A}General formalism}{22}{section*.47}
\contentsline {subsubsection}{\numberline {1}Low-scale limits}{22}{section*.48}
\contentsline {subsubsection}{\numberline {2}High-scale limits}{22}{section*.49}
\contentsline {subsubsection}{\numberline {3}Mixed low- and high-scale limits}{22}{section*.50}
\contentsline {subsubsection}{\numberline {4}Hatree-Fock constraints}{22}{section*.51}
\contentsline {subsection}{\numberline {B}Direct application to ultracold atoms systems III: effective range effect }{23}{section*.52}
\appendix 
\contentsline {section}{\numberline {A}Another method for ultracold atoms systems based on symmetry at Hartree-Fock level }{23}{section*.53}
\contentsline {subsection}{\numberline {1}Some consideration about the Bertsch parameter}{23}{section*.54}
\contentsline {subsection}{\numberline {2}Construction of the two-body interaction at HF level}{23}{section*.55}
\contentsline {subsubsection}{\numberline {a}Low density}{24}{section*.56}
\contentsline {subsubsection}{\numberline {b}Strict unitarity}{24}{section*.57}
\contentsline {subsubsection}{\numberline {c}Expansion close to unitarity}{24}{section*.58}
\contentsline {subsubsection}{\numberline {d}HF energy}{24}{section*.59}
\contentsline {subsection}{\numberline {3}Interaction in coordinate-space}{24}{section*.60}
\contentsline {subsection}{\numberline {4}Quasi-particle properties}{25}{section*.61}
\contentsline {subsubsection}{\numberline {a}Chemical potential}{26}{section*.62}
\contentsline {subsubsection}{\numberline {b}Effective mass}{26}{section*.63}
\contentsline {subsection}{\numberline {5}Second and third order effective MBPT at HF level }{26}{section*.64}
\contentsline {subsection}{\numberline {6}Discussion on perturbation theory with a correlated HF effective interaction}{27}{section*.65}
\contentsline {subsubsection}{\numberline {a}Subtraction scheme at low density}{27}{section*.66}
\contentsline {subsubsection}{\numberline {b}Subtraction scheme at unitarity}{27}{section*.67}
\contentsline {subsubsection}{\numberline {c}Can we make perturbative expansion in both direction at same order?}{28}{section*.68}
\contentsline {subsection}{\numberline {7}MBPT at second order}{28}{section*.69}
\contentsline {subsubsection}{\numberline {a}Determination of $\mathaccentV {bar}016{\xi _0}$}{28}{section*.70}
\contentsline {subsubsection}{\numberline {b}Calculation of the second order MBPT correction}{28}{section*.71}
\contentsline {section}{\numberline {B}Generalization of the limiting high-scale constraint}{30}{section*.72}
\contentsline {subsection}{\numberline {1}Multi-constrained IMCA at HF level}{30}{section*.73}
\contentsline {subsubsection}{\numberline {a}Application to ultracold atoms systems}{30}{section*.74}
\contentsline {subsection}{\numberline {2}From discrete to continuous multi-constraints}{30}{section*.75}
\contentsline {subsubsection}{\numberline {a}Determination of the continuum functions $P$ and $T$}{31}{section*.76}
\contentsline {subsubsection}{\numberline {b}Determination of the continuum functions $S$}{31}{section*.77}
\contentsline {subsection}{\numberline {3}Study of the auto-consistent equation}{31}{section*.78}
\contentsline {subsubsection}{\numberline {a}Minimal constraint}{31}{section*.79}
\contentsline {subsubsection}{\numberline {b}Maximal constraint}{31}{section*.80}
\contentsline {subsubsection}{\numberline {c}Solution Fredholm equation using the resolvent formalism}{31}{section*.81}
\contentsline {subsubsection}{\numberline {d}Change of variable and integration by part}{32}{section*.82}
\contentsline {section}{\numberline {C}Scaling invariance}{33}{section*.83}
\contentsline {section}{\numberline {}References}{34}{section*.84}
