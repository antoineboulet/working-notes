\contentsline {title}{Variational perturbation theory \\ for \emph {ab initio} density functional theory: case study}{1}{section*.2}%
\contentsline {abstract}{Abstract}{1}{section*.1}%
\tocdepth@munge 
\contentsline {section}{\numberline {}Contents}{1}{section*.3}%
\tocdepth@restore 
\contentsline {section}{\numberline {I}Preliminary results}{1}{section*.4}%
\contentsline {subsection}{\numberline {A}Effective action formalism and generating partition function}{2}{section*.5}%
\contentsline {subsubsection}{\numberline {1}Many-body Hamiltonian}{2}{section*.6}%
\contentsline {subsubsection}{\numberline {2}Action and generating functional}{2}{section*.7}%
\contentsline {subsubsection}{\numberline {3}Auxiliary classical collective fields}{2}{section*.8}%
\contentsline {subsection}{\numberline {B}One-body Green's functions}{2}{section*.9}%
\contentsline {subsection}{\numberline {C}Wigner transform and gradient expansion of the one-body Green's functions }{3}{section*.10}%
\contentsline {subsubsection}{\numberline {1}Generalities on the Wgner transform}{3}{section*.11}%
\contentsline {subsubsection}{\numberline {2}Grandient expansion of the one-body Green's function}{3}{section*.12}%
\contentsline {subsubsection}{\numberline {3}Functional derivative of the zero order effective action and generating one-body Green's function}{4}{section*.13}%
\contentsline {subsection}{\numberline {D}Generating effective action }{4}{section*.14}%
\contentsline {subsubsection}{\numberline {1}Generating effective action for the non-interacting system}{4}{section*.15}%
\contentsline {subsubsection}{\numberline {2}Estimation of the zero-order effective action in the Hartree-Fock approximation }{5}{section*.16}%
\contentsline {section}{\numberline {II}First order in perturbation: Hartree-Fock level}{5}{section*.17}%
\contentsline {subsection}{\numberline {A}Self-consistent Hartree-Fock equation using the gradient expansion of the Green's functions }{6}{section*.18}%
\contentsline {paragraph}{\numberline {a}First iteration --}{7}{section*.19}%
\contentsline {paragraph}{\numberline {b}Recurrence relation --}{7}{section*.20}%
\contentsline {paragraph}{\numberline {c}Application --}{7}{section*.21}%
\contentsline {subsection}{\numberline {B}Gradient expansion of the density matrix and link with the density matrix expansion }{8}{section*.22}%
\contentsline {subsubsection}{\numberline {1}Explicit form of the one-body density matrix up to second order in the gradient expansion approximation}{9}{section*.23}%
\contentsline {subsubsection}{\numberline {2}Local normal, kinetic and current}{9}{section*.24}%
\contentsline {subsubsection}{\numberline {3}Exact expansion of the one-body density matrix in terms of the densities}{9}{section*.25}%
\contentsline {subsubsection}{\numberline {4}Density functional}{10}{section*.26}%
\contentsline {section}{\numberline {III}Density functional theory}{10}{section*.27}%
\contentsline {subsection}{\numberline {A}Contribution of the interacting part: density-dependent coupling constants}{10}{section*.28}%
\contentsline {subsubsection}{\numberline {1}Hartree contribution}{11}{section*.29}%
\contentsline {subsubsection}{\numberline {2}Fock contribution}{11}{section*.30}%
\contentsline {subsection}{\numberline {B}Non-interacting part: reference single-particle states}{11}{section*.31}%
\contentsline {subsubsection}{\numberline {1}Kohn-Sham equations}{11}{section*.32}%
\contentsline {subsubsection}{\numberline {2}Effective local density-dependent interaction}{12}{section*.33}%
\contentsline {subsection}{\numberline {C}Local kinetic density as a functional of the reference local density}{12}{section*.34}%
\contentsline {subsection}{\numberline {D}Density-dependent single-particle potential}{12}{section*.35}%
\appendix 
\contentsline {section}{\numberline {A}Kohn-Sham potential }{13}{section*.36}%
\contentsline {subsection}{\numberline {1}One-body density matrix expansion}{13}{section*.37}%
\contentsline {subsubsection}{\numberline {a}Local densities}{13}{section*.38}%
\contentsline {subsubsection}{\numberline {b}Local functionals}{13}{section*.39}%
\contentsline {subsubsection}{\numberline {c}Non-local functions}{13}{section*.40}%
\contentsline {subsection}{\numberline {2}Hartree-Fock coupling constants}{13}{section*.41}%
\contentsline {subsection}{\numberline {3}Density-dependent auxiliary field}{13}{section*.42}%
\contentsline {subsubsection}{\numberline {a}Density-dependent effective mass}{13}{section*.43}%
\contentsline {subsubsection}{\numberline {b}Residual self-energy}{14}{section*.44}%
\contentsline {subsection}{\numberline {4}Local functional correction to the local densities}{14}{section*.45}%
\contentsline {subsection}{\numberline {5}Funcional derivatives}{14}{section*.46}%
\contentsline {subsubsection}{\numberline {a}Functional derivative of the local functions}{14}{section*.47}%
\contentsline {subsubsection}{\numberline {b}Functional derivative of the non-local coupling functions}{14}{section*.48}%
\contentsline {subsubsection}{\numberline {c}Functional derivative of the effective mass and residual self-energy}{15}{section*.49}%
\contentsline {section}{\numberline {B}Fourier and Wigner transform conventions }{16}{section*.50}%
\contentsline {subsection}{\numberline {1}Change of variable convention}{16}{section*.51}%
\contentsline {paragraph}{\numberline {}Time}{16}{section*.52}%
\contentsline {paragraph}{\numberline {}Coordinate}{16}{section*.53}%
\contentsline {paragraph}{\numberline {}Frequency}{16}{section*.54}%
\contentsline {paragraph}{\numberline {}Momentum}{16}{section*.55}%
\contentsline {subsection}{\numberline {2}Fourier transform in the time-frequency domain}{16}{section*.56}%
\contentsline {subsection}{\numberline {3}Wigner transformation}{16}{section*.57}%
\contentsline {subsection}{\numberline {4}Specific integrals}{16}{section*.58}%
\contentsline {subsubsection}{\numberline {a}Constant function and Dirac distributions}{16}{section*.59}%
\contentsline {subsubsection}{\numberline {b}Heaviside distribution}{16}{section*.60}%
\contentsline {section}{\numberline {C}Contour integration }{16}{section*.61}%
\contentsline {subsection}{\numberline {1}Usful integral implicating Dirac and Heaviside distribution }{16}{section*.62}%
\contentsline {section}{\numberline {D}Functional derivative }{17}{section*.63}%
\contentsline {paragraph}{\numberline {}Linearity}{17}{section*.64}%
\contentsline {paragraph}{\numberline {}Product rule}{17}{section*.65}%
\contentsline {paragraph}{\numberline {}Chain rule}{17}{section*.66}%
\contentsline {section}{\numberline {E}Generated one-body Green's function }{17}{section*.67}%
\contentsline {subsection}{\numberline {1}Mathematical relation on matrix derivative}{18}{section*.68}%
\contentsline {section}{\numberline {}References}{19}{section*.69}%
