\contentsline {title}{Variational perturbation theory \\ for \emph {ab initio} density functional theory}{1}{section*.2}%
\contentsline {abstract}{Abstract}{1}{section*.1}%
\tocdepth@munge 
\contentsline {section}{\numberline {}Contents}{1}{section*.3}%
\tocdepth@restore 
\contentsline {section}{\numberline {I}General formalism}{2}{section*.4}%
\contentsline {subsection}{\numberline {A}Many-body Hamiltonian}{2}{section*.5}%
\contentsline {subsection}{\numberline {B}Action and generating functional}{2}{section*.6}%
\contentsline {subsection}{\numberline {C}Auxiliary classical collective fields}{2}{section*.7}%
\contentsline {subsection}{\numberline {D}Construction of the effective classical action}{3}{section*.8}%
\contentsline {subsubsection}{\numberline {1}Definition of the reference state}{3}{section*.9}%
\contentsline {subsubsection}{\numberline {2}Perturbation theory }{3}{section*.10}%
\contentsline {subsection}{\numberline {E}Functional derivatives of the classical action}{4}{section*.11}%
\contentsline {paragraph}{\numberline {}Linearity}{4}{section*.12}%
\contentsline {paragraph}{\numberline {}Product rule}{4}{section*.13}%
\contentsline {paragraph}{\numberline {}Chain rule}{4}{section*.14}%
\contentsline {subsubsection}{\numberline {1}Auxiliary contribution}{4}{section*.15}%
\contentsline {subsubsection}{\numberline {2}Two-body contribution}{4}{section*.16}%
\contentsline {subsubsection}{\numberline {3}Three-body contribution}{5}{section*.17}%
\contentsline {subsection}{\numberline {F}Extremization procedure with local interaction}{5}{section*.18}%
\contentsline {section}{\numberline {II}Beyond variational Hartree-Fock-Bogoliubov}{5}{section*.19}%
\contentsline {subsection}{\numberline {A}Partition function and effective action}{5}{section*.20}%
\contentsline {subsection}{\numberline {B}Schematic Wick's contraction}{6}{section*.21}%
\contentsline {subsection}{\numberline {C}Extremisation procedure}{6}{section*.22}%
\contentsline {section}{\numberline {III}Diagrammatic and Feynmann rules}{7}{section*.23}%
\contentsline {subsection}{\numberline {A}Second order diagrams}{7}{section*.24}%
\contentsline {subsection}{\numberline {B}First order of the perturbation}{9}{section*.25}%
\contentsline {subsubsection}{\numberline {1}First extremization procedure}{9}{section*.26}%
\contentsline {subsubsection}{\numberline {2}Second extremization procedure}{9}{section*.27}%
\contentsline {subsection}{\numberline {C}Second order in perturbation}{9}{section*.28}%
\contentsline {subsubsection}{\numberline {1}Reduction procedure}{10}{section*.29}%
\contentsline {subsubsection}{\numberline {2}Determination of the density propagators}{11}{section*.30}%
\contentsline {section}{\numberline {IV}Gradient expansion of the one-body Green's functions }{12}{section*.31}%
\contentsline {section}{\numberline {V}Application to a simplified model}{13}{section*.32}%
\contentsline {subsection}{\numberline {A}Effective action formalism and generating partition function}{13}{section*.33}%
\contentsline {subsubsection}{\numberline {1}Many-body Hamiltonian}{13}{section*.34}%
\contentsline {subsubsection}{\numberline {2}Action and generating functional}{13}{section*.35}%
\contentsline {subsubsection}{\numberline {3}Auxiliary classical collective fields}{14}{section*.36}%
\contentsline {subsection}{\numberline {B}One-body Green's functions}{14}{section*.37}%
\contentsline {subsubsection}{\numberline {1}Grandient expansion of the one-body Green's function}{14}{section*.38}%
\contentsline {subsubsection}{\numberline {2}Functional derivative of the zero order effective action and generating one-body Green's function}{15}{section*.39}%
\contentsline {subsection}{\numberline {C}First order in perturbation: Hartree-Fock level}{15}{section*.40}%
\contentsline {section}{\numberline {VI}Effective action using the zero-order gradient expansion of the one-body Green's function: infinite mater study}{16}{section*.41}%
\contentsline {subsubsection}{\numberline {1}Solution of the auxiliary field at extremum using the zero-order gradient expansion of the one-body Green's function}{16}{section*.42}%
\contentsline {paragraph}{\numberline {a}First iteration --}{16}{section*.43}%
\contentsline {paragraph}{\numberline {b}Second iteration --}{17}{section*.44}%
\contentsline {paragraph}{\numberline {c}Recurence relation --}{17}{section*.45}%
\contentsline {subsubsection}{\numberline {2}Estimation of the zero-order effective action using the zero-order gradient expansion of the one-body Green's function: non-interacting system}{17}{section*.46}%
\contentsline {subsubsection}{\numberline {3}Estimation of the zero-order effective action using the zero-order gradient expansion of the one-body Green's function: infinite interacting system}{18}{section*.47}%
\contentsline {subsubsection}{\numberline {4}Application to the simplest contact interaction model}{18}{section*.48}%
\contentsline {section}{\numberline {VII}Effective action using the zero-order gradient expansion of the one-body Green's function for finite systems: local density approximation (Thomas-Fermi approximation)}{19}{section*.49}%
\contentsline {paragraph}{\numberline {a}First iteration --}{19}{section*.50}%
\contentsline {paragraph}{\numberline {b}Second iteration --}{19}{section*.51}%
\contentsline {paragraph}{\numberline {c}Recurence relation --}{19}{section*.52}%
\contentsline {section}{\numberline {VIII}Effective action using the second-order gradient expansion of the one-body Green's function for finite systems: beyond local density approximation}{19}{section*.53}%
\contentsline {subsection}{\numberline {A}Link with the density matrix expansion}{19}{section*.54}%
\contentsline {subsubsection}{\numberline {1}General discussion}{19}{section*.55}%
\contentsline {subsubsection}{\numberline {2}Concept of the densiy matrix expanssion}{21}{section*.56}%
\contentsline {section}{\numberline {IX}Beyond Hartree-Fock calculation: second order in perturbation}{22}{section*.57}%
\appendix 
\contentsline {section}{\numberline {A}Fourier and Wigner transform conventions }{22}{section*.58}%
\contentsline {subsection}{\numberline {1}Change of variable convention}{22}{section*.59}%
\contentsline {paragraph}{\numberline {}Time}{22}{section*.60}%
\contentsline {paragraph}{\numberline {}Coordinate}{22}{section*.61}%
\contentsline {paragraph}{\numberline {}Frequency}{22}{section*.62}%
\contentsline {paragraph}{\numberline {}Momentum}{22}{section*.63}%
\contentsline {subsection}{\numberline {2}Fourier transform in the time-frequency domain}{22}{section*.64}%
\contentsline {subsection}{\numberline {3}Wigner transformation}{22}{section*.65}%
\contentsline {subsection}{\numberline {4}Specific integrals}{22}{section*.66}%
\contentsline {subsubsection}{\numberline {a}Constant function and Dirac distributions}{22}{section*.67}%
\contentsline {subsubsection}{\numberline {b}Heaviside distribution}{22}{section*.68}%
\contentsline {section}{\numberline {B}Contour integration }{22}{section*.69}%
\contentsline {subsection}{\numberline {1}Usful integral implicating Dirac and Heaviside distribution }{23}{section*.70}%
\contentsline {section}{\numberline {C}Functional derivative}{23}{section*.71}%
\contentsline {paragraph}{\numberline {}Linearity}{23}{section*.72}%
\contentsline {paragraph}{\numberline {}Product rule}{23}{section*.73}%
\contentsline {paragraph}{\numberline {}Chain rule}{23}{section*.74}%
\contentsline {section}{\numberline {D}Generated one-body Green's function }{23}{section*.75}%
\contentsline {subsection}{\numberline {1}Mathematical relation on matrix derivative}{24}{section*.76}%
\contentsline {section}{\numberline {E}Self-consistent Hartree-Fock equation using the gradient expansion of the Green's functions }{25}{section*.77}%
\contentsline {paragraph}{\numberline {a}First iteration --}{25}{section*.78}%
\contentsline {paragraph}{\numberline {b}Recurrence relation --}{26}{section*.79}%
\contentsline {subsection}{\numberline {1}Local density}{26}{section*.80}%
\contentsline {subsection}{\numberline {2}One-body density matrix expansion}{26}{section*.81}%
\contentsline {subsubsection}{\numberline {a}Link with the density matrix expansion}{26}{section*.82}%
\contentsline {section}{\numberline {F}Include IM-SRG methods}{27}{section*.83}%
\contentsline {subsubsection}{\numberline {a}Similarity Renormalization Group}{27}{section*.84}%
\contentsline {subsubsection}{\numberline {b}In-Medium Similarity Renormalization Group}{27}{section*.85}%
\contentsline {subsubsection}{\numberline {c}Auxiliary classical collective fields with normal ordering}{28}{section*.86}%
\contentsline {section}{\numberline {G}Numerical aspects of gradient generalized non-linear multi-variable Fredholm integral equations}{}{section*.87}%
\contentsline {section}{\numberline {}References}{}{section*.88}%
