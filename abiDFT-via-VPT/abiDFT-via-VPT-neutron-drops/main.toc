\contentsline {title}{Variational perturbation theory for \emph {ab initio} density functional theory: \\ application to neutron drops with semi-realistic interaction}{1}{section*.2}%
\contentsline {abstract}{Abstract}{1}{section*.1}%
\tocdepth@munge 
\contentsline {section}{\numberline {}Contents}{1}{section*.3}%
\tocdepth@restore 
\contentsline {section}{\numberline {I}Article for neutron drops}{1}{section*.4}%
\contentsline {section}{\numberline {II}Preliminary results}{2}{section*.5}%
\contentsline {subsection}{\numberline {A}Effective action formalism and generating partition function}{2}{section*.6}%
\contentsline {subsubsection}{\numberline {1}Many-body Hamiltonian}{2}{section*.7}%
\contentsline {subsubsection}{\numberline {2}Action and generating functional}{3}{section*.8}%
\contentsline {subsubsection}{\numberline {3}Auxiliary classical collective fields}{3}{section*.9}%
\contentsline {subsection}{\numberline {B}One-body Green's functions}{3}{section*.10}%
\contentsline {subsection}{\numberline {C}Wigner transform and gradient expansion of the one-body Green's functions }{4}{section*.11}%
\contentsline {subsubsection}{\numberline {1}Generalities on the Wgner transform}{4}{section*.12}%
\contentsline {subsubsection}{\numberline {2}Grandient expansion of the one-body Green's function}{4}{section*.13}%
\contentsline {subsubsection}{\numberline {3}Functional derivative of the zero order effective action and generating one-body Green's function}{5}{section*.14}%
\contentsline {section}{\numberline {III}First order in perturbation: Hartree-Fock level}{5}{section*.15}%
\contentsline {subsection}{\numberline {A}Self-consistent Hartree-Fock equation using the gradient expansion of the Green's functions }{5}{section*.16}%
\contentsline {subsection}{\numberline {B}Gradient expansion of the density matrix and link with the density matrix expansion }{6}{section*.17}%
\contentsline {subsubsection}{\numberline {1}Local anisotropie of the Fermi surface}{6}{section*.18}%
\contentsline {subsubsection}{\numberline {2}Local effective mass and residual self-energy}{7}{section*.19}%
\contentsline {subsubsection}{\numberline {3}Inverse Wigner transform of the one-body density matrix}{7}{section*.20}%
\contentsline {subsubsection}{\numberline {4}Gradient angle dependance}{8}{section*.21}%
\contentsline {subsubsection}{\numberline {5}Local normal and kinetic densities}{8}{section*.22}%
\contentsline {subsubsection}{\numberline {6}Exact expansion of the one-body density matrix in terms of the densities}{8}{section*.23}%
\contentsline {subsubsection}{\numberline {7}Density functional}{9}{section*.24}%
\contentsline {section}{\numberline {IV}Density functional theory }{9}{section*.25}%
\contentsline {subsection}{\numberline {A}Non-interacting part, Hartree-Fock part, and total ground state energy}{10}{section*.26}%
\contentsline {subsection}{\numberline {B}KS equations (without qp properties)}{11}{section*.27}%
\contentsline {subsubsection}{\numberline {1}Kinetic contribution}{11}{section*.28}%
\contentsline {subsubsection}{\numberline {2}External contribution}{11}{section*.29}%
\contentsline {subsubsection}{\numberline {3}Internal contribution}{11}{section*.30}%
\contentsline {subsubsection}{\numberline {4}Ground state energy as a density functional}{11}{section*.31}%
\contentsline {subsubsection}{\numberline {5}Kohn-Sham equations}{11}{section*.32}%
\contentsline {subsection}{\numberline {C}KS equations (with qp properties)}{12}{section*.33}%
\contentsline {subsubsection}{\numberline {1}Effective mass contribution}{12}{section*.34}%
\contentsline {subsubsection}{\numberline {2}Residual qp contribution}{12}{section*.35}%
\contentsline {subsubsection}{\numberline {3}Ground state energy as a density functional}{12}{section*.36}%
\contentsline {subsubsection}{\numberline {4}Kohn-Sham equations}{12}{section*.37}%
\contentsline {subsection}{\numberline {D}Critical discusion}{12}{section*.38}%
\contentsline {section}{\numberline {V}VPT: resolution of HF}{13}{section*.39}%
\appendix 
\contentsline {section}{\numberline {A}Two-body interaction kernel }{14}{section*.40}%
\contentsline {section}{\numberline {B}One-body density matrix}{15}{section*.41}%
\contentsline {subsection}{\numberline {1}Local functions}{15}{section*.42}%
\contentsline {subsubsection}{\numberline {a}The $f_i$ functions}{15}{section*.43}%
\contentsline {subsubsection}{\numberline {b}The $w_i$, auxiliary field, and effective mass functionals}{15}{section*.44}%
\contentsline {paragraph}{\numberline {a}Auxiliary field}{15}{section*.45}%
\contentsline {paragraph}{\numberline {b}Effective mass}{16}{section*.46}%
\contentsline {paragraph}{\numberline {c}$w_i$ function}{16}{section*.47}%
\contentsline {subsection}{\numberline {2}Non-local functions}{16}{section*.48}%
\contentsline {subsubsection}{\numberline {a}Angle average}{16}{section*.49}%
\contentsline {subsubsection}{\numberline {b}Angle average in coupling constants}{16}{section*.50}%
\contentsline {section}{\numberline {C}Fourier and Wigner transform conventions }{17}{section*.51}%
\contentsline {subsection}{\numberline {1}Change of variable convention}{17}{section*.52}%
\contentsline {paragraph}{\numberline {}Time}{17}{section*.53}%
\contentsline {paragraph}{\numberline {}Coordinate}{17}{section*.54}%
\contentsline {paragraph}{\numberline {}Frequency}{17}{section*.55}%
\contentsline {paragraph}{\numberline {}Momentum}{17}{section*.56}%
\contentsline {subsection}{\numberline {2}Fourier transform in the time-frequency domain}{17}{section*.57}%
\contentsline {subsection}{\numberline {3}Wigner transformation}{17}{section*.58}%
\contentsline {subsection}{\numberline {4}Specific integrals}{17}{section*.59}%
\contentsline {subsubsection}{\numberline {a}Constant function and Dirac distributions}{17}{section*.60}%
\contentsline {subsubsection}{\numberline {b}Heaviside distribution}{17}{section*.61}%
\contentsline {section}{\numberline {D}Contour integration }{17}{section*.62}%
\contentsline {subsection}{\numberline {1}Usful integral implicating Dirac and Heaviside distribution }{17}{section*.63}%
\contentsline {section}{\numberline {E}Functional derivative }{18}{section*.64}%
\contentsline {paragraph}{\numberline {}Linearity}{18}{section*.65}%
\contentsline {paragraph}{\numberline {}Product rule}{18}{section*.66}%
\contentsline {paragraph}{\numberline {}Chain rule}{18}{section*.67}%
\contentsline {section}{\numberline {F}Generated one-body Green's function }{18}{section*.68}%
\contentsline {section}{\numberline {}References}{19}{section*.69}%
