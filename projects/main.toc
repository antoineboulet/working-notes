\contentsline {title}{Postdoctoral projects at MSU: research directions and achievements}{1}{section*.2}
\contentsline {abstract}{Abstract}{1}{section*.1}
\tocdepth@munge 
\contentsline {section}{\numberline {}Contents}{1}{section*.3}
\tocdepth@restore 
\contentsline {section}{\numberline {I}Density Matrix Expanstion (DME)}{1}{section*.4}
\contentsline {subsection}{\numberline {A}Chiral 3N contributions}{2}{section*.5}
\contentsline {subsection}{\numberline {B}DME beyond HF}{2}{section*.6}
\contentsline {subsection}{\numberline {C}Other issues}{3}{section*.7}
\contentsline {subsubsection}{\numberline {1}Other constraints}{3}{section*.8}
\contentsline {subsubsection}{\numberline {2}Regulators issues}{3}{section*.9}
\contentsline {subsubsection}{\numberline {3}Possible speculative extensions}{3}{section*.10}
\contentsline {section}{\numberline {II}In-Medium Similarity Renormalization Group (IMSRG)}{3}{section*.11}
\contentsline {subsection}{\numberline {A}DFT from effective actions}{3}{section*.12}
\contentsline {section}{\numberline {III}DFT for nuclear EDF}{3}{section*.13}
\contentsline {section}{\numberline {IV}Resummation}{3}{section*.14}
\contentsline {section}{\numberline {V}From cold atoms to nuclear physics}{3}{section*.15}
\contentsline {subsection}{\numberline {A}Continuity of the PhD}{3}{section*.16}
\contentsline {subsubsection}{\numberline {1}Pairing and resummation}{3}{section*.17}
\contentsline {subsubsection}{\numberline {2}$\omega $-dependence of the resummed self-energy}{3}{section*.18}
\contentsline {subsubsection}{\numberline {3}Applications to finite systems (quantum drop) with density-dependent Skyrme-like functional}{4}{section*.19}
\contentsline {subsection}{\numberline {B}MBPT extended with constraints }{4}{section*.20}
\contentsline {section}{\numberline {VI}Possible collaboration with QMC community}{4}{section*.21}
\contentsline {section}{\numberline {VII}\emph {Ab initio} calculations for semi-infinite matter}{4}{section*.22}
\contentsline {section}{\numberline {VIII}Symmetry broken and restored self-consistent Green's function theory}{4}{section*.23}
\contentsline {section}{\numberline {}References}{4}{section*.24}
