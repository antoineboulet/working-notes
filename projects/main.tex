\documentclass[reprint,amsmath,amssymb,aps,rmp,showkeys,floatfix]{revtex4-1}
 % superscriptaddress                % useful for collaboration
 % \usepackage{catchfilebetweentags} % useful for collaboration
\input{../header_notes.tex} \input{../bibliography_style.tex} \input{../abreviation.tex}  \input{../math_commands.tex}


\begin{document} \input{FrontBackMatter.tex}


\section{Density Matrix Expanstion (DME)}

The use of contact interactions was justified by Negele and Vautherin \cite{Negele1972a,Negele1975a} starting from a microscopic point of view.
The approach they used is the density matrix expansion (\dme) directly in a many-body calculation to map finite-range physics into a quasi-local generalized Skyrme-like contact interaction.
Even more, it suggests a systematic generalization of functionals in terms of higher derivatives of the density.
For instance the next-to-leading order (\nlo), \ie the \dme up to second order in derivative of the density, reveals the same structure as the standard Skyrme effective interaction\footnote{Note that the leading order (\lo) corresponds to the term in ($t_0,x_0$) only \cite{Carlsson2008a,Raimondi2011a}.}.
Nevertheless, Negele itself in \cite{Negele1975b} (see also \cite{Sprung1975a}) discussed the limitation of the approach and concluded that the density-dependent Skyrme-like effective interaction is not efficient because of the lack of generality compared to the \dme results.
One can mention that the recent progress in \abi methods have provided a renewed interest on the \dme and have reached now a certain level of maturity
\cite{Gebremariam2010a,Stoitsov2010a,Carlsson2010a,Gebremariam2011a,Bogner2011a,Dyhdalo2017a,Carlsson2008a,Carlsson2010b,Dobaczewski2010a,Erler2010a,Raimondi2011a,Navarro2018a,Navarro2019a,Bogner2009a}
among which we can distinguish several approaches:
\begin{itemize}
  \item Negele \& Vautherin \cite{Negele1972a,Negele1975a,Negele1975b} (see also \cite{Dobaczewski2010a}).
  Here, the authors focus manly on the scalar part of the one body density matrix (\obdm). In particular, their are not able to find an approximation at the same level for the vector part of the \obdm. Consequently, the \nv-\dme describe essentially the spin-saturated systems.
  %
  \item Bogner \etal
  \cite{Bogner2009a,Bogner2011a,Gebremariam2010a,Gebremariam2011a,Dyhdalo2017a,Stoitsov2010a,Navarro2018a,Navarro2019a}.
  These work, formulated in \cite{Gebremariam2010a}, is based on  phase-space-averaging (\psa). This allow to get approximation for the scalar and vector part of the \obdm at the same level\footnote{Note that in \cite{Navarro2018a,Navarro2019a,Dyhdalo2017a} the direct term is treated ``exactly'' in the sense that the authors use several gaussian (as for the Gogny force) to approximate the chiral interaction.}; and, consequently, allow the description of spin-unsaturated systems. This also offer a better reproduction of the exchange/Fock contributions.
  As summarized in \cite{Gebremariam2010a} the \psa methods relies on three steps:
  \begin{enumerate}
    \item the isolation of the non-locality as an operator $\mathcal{O}$ acting on the \obdm;
    \item the expansion of $\mathcal{O}$ around a momentum scale $\bm{k}$;
    \item the averaging of $\bm{k}$ over the local momentum distribution of the system.
  \end{enumerate}
  Note also that two \texttt{Mathematica} package are presented in \cite{Gebremariam2010b,Gebremariam2010c} which may be useful for this project.
  %
  \item Dobaczewski \etal \cite{Carlsson2008a,Carlsson2010a,Carlsson2010b,Raimondi2011a}. The approach developed here consist in, given general symmetries of the system (Galilean invariance, \etc), construct/design the most general form of the effective interaction at a given order. Here the studies was developed up to \nnnlo; for instance the \nlo functional can be identified to the standard Skyrme parametrization.
  %
  \item Reinhard \etal \cite{Erler2010a}. In this work, the authors propose an additional term in the Skyrme functional as rational approximant of the density-dependent term $\rho^\alpha \to (a\rho+b\rho^2)/(1+a\rho)$. The advantage is to have integer powers of the density allowing particle-number projection. This approach is essentially phenomenological but use some term similar to what we can get using the \dme.
\end{itemize}

We can found some working notes on the \hf theory and generalities on the \dme at \url{https://gitlab.com/antoineboulet/working-notes} \texttt{[generalities-on-DME]}: \href{../generalities-on-DME/main.pdf}{\emph{Generalities on the density matrix expansion}}.

In \cite{Navarro2018a} (see also \cite{Stoitsov2010a,Gebremariam2011a,Gebremariam2010a,Bogner2011a,Dyhdalo2017a} and \cite{Bogner2009a}), we can see the basic idea of the approach along with some (potentially) promising early results.
There are several open questions from this paper that we would like to answer during the postdoctoral projects.


\subsection{Chiral 3N contributions}

\begin{enumerate}
  \item
  \emph{Why is there no improvement\footnote{In fact, a slight worsening.} with the addition of the chiral \nnn contributions?}

  There is an abundance of evidence from \abi calculations that \nnn forces play a decisive role (the evolution of structure along isotopic chains, saturation properties, \etc).
  There is also a physical argument based on spin orbit physics-microscopically, while spin-orbit splittings are indeed dominantly from short-range \nn forces, it has long been known that the long-range \nnn and iterated \nn (tensor force) still play an important role.
  Naively, since Skyrme/Gogny model this with just a zero-range density independent contact term, one might think the \dme functionals might give improved/different single particle properties since they account for these different sources of spin-orbit splitting but the results in the above paper do not really show any substantial differences.
  %
  \item
  Related to the first point, the accuracy of the \nnn \dme has never properly been benchmarked as has been done with the \nn case.
  A straightforward calculation would be to take some \hf single particle wave functions and evaluate the \hf interaction energy $\langle V_3N \rangle$ exactly, and then compare the same quantity evaluated within the \dme.
  See the \href{../../bibliography/thesis/Dyhdalo_PhD2018.pdf}{\texttt{PhD thesis}} of Alex Dyhdalo
  for more details on the improved formulation of the \dme used in these works.
\end{enumerate}

In \cite{Davesne2018a}, the authors show the fact that even if we enrich the (effective) two-body interaction with higher order contributions, the Hartree-Fock level calculation leads to a underestimated effective mass of symmetric nuclear matter.
Only the introduction of a density dependent terms $(t_3,\rho^\alpha)$ guarantee to get the expected range. But the fitting procedure to fix such parameter are strongly correlated.
It seems to me that this work is a good starting point to understand why the \dme (at \hf level) is not improved by adding higher order in the interaction (concerning the two-body part). For the three-body force (simulated by the density-dependent term in this work), it could have an issue on the fitting procedure.
To conclude, I have the feeling that we can understand by simple argument why the \dme is not improved staying at \hf level.



\subsection{DME beyond HF}

\emph{How can we generalize the \dme to “beyond Hartree-Fock” contributions to the energy?}

In the above paper \cite{Navarro2018a} and its predecessors, the authors sidestep this problem by a handwaving argument.
Basically, they argue that since the dominant bulk contributions to ground state energies are of the Brueckner Hartree-Fock form, and since the $G$-matrix differs from the bare \nn interaction only at short distances due to the “healing” property, one can apply the \dme to the long-range part of the \hf energy (no adjustable parameters since it is all fixed by well understood long-distance physics).
Then one can add these contributions to the Skyrme functional, and then refit the Skyrme parameters.

The refit Skyrme parameters are absorbing the effects from both the short-range beyond \hf correlations and the omitted \eft contact interactions at \hf level.
While the handwaving argument is more or less correct, it is by definition an \adhoc approach.
We would like to make it systematic somehow. One rough idea Scott have is to use the operator product expansion and “universality” of short-range correlations as speculated on in the end of his \href{../../bibliography/talks/Bogner_INT2019.pdf}{\texttt{INT talk}}.
Incidentally, this factorization of long- and short-distance physics reminds a little of so-called ``Range separated \dft'' approaches in coulomb \dft.


\subsection{Other issues}

\subsubsection{Other constraints}

\begin{itemize}
  \item What is about semi-infinite nuclear matter constraints (which are usually used to adjust standard \edf and which are even present in the liquid drop model)?
  %I suspect [to be clarify] it is an important aspect that could be explain partially why the \nnn contribution do not improve so much the results.
  But, as mention in \cite{Stoitsov2010a}, such adjustment is not directly allowed by the \dme formulation.
\end{itemize}


\subsubsection{Regulators issues}

It seems to me quite artificial, I have to understand this issues deeper. For instance, why use in particular the form (1) in \cite{Navarro2018a} with $n=6$?


\subsubsection{Possible speculative extensions}

Below I list some possible extension of the \dme formulation.
\begin{itemize}
  \item Adding anomalous density matrix to include the paring field consistently.
  %
  \item Introduce off-shell effect.
  %
  \item Use gradient density-dependent terms (in denominator) to define the coupling.
\end{itemize}



\section{In-Medium Similarity Renormalization Group (IMSRG)}

\emph{Collaborators: Scott Bogner, Dick Furnstahl, Heiko Hergert.}

In more speculative directions, the use of in-medium similarity renormalization group (\imsrg) could be possible to microscopically construct \edfs, analogous to so-called optimized effective potential and effective action methods in Coulomb \dft.


\subsection{DFT from effective actions}
\emph{Collaborators: Dick Furnstahl, Scott Bogner (also Jean-Paul Ebran, Thomas Duguet \etal).}

For instance, see \cite{Furnstahl2019a}.




\section{DFT for nuclear EDF}

\emph{Collaborator: Kévin Fossez.}

Expanding the chiral EFT (first the pionless EFT as a proof-of principle) around an averaged momentum of the systems by including the Fermi momentum $k_F$ and the radius of the finite systems $R_0$ (unknown) to express the effective interaction as in pionless EFT and include the central potential required to obtain a proper nuclear DFT well defined in the Kohn-Sham formalism.




\section{Resummation}
\emph{Are there other idealized limits (other than the unitary limit), \eg large $N_c$, chiral limit, \etc, that can be exploited?}

The Coulomb \dft (see the work of Perdew \etal) can use various exact relations that the functional must have, \eg under uniform scaling of coordinates, to constrain the form of the phenomenological functionals.
Part of this stems from the extreme simplicity (absent for realistic nuclear forces) of the Coulomb force, \eg the scaling relations follow from the simple power law form of the interaction.
A first question we could address: is such simplicity can be recovered in the chiral or large $N_c$ limits?


\section{From cold atoms to nuclear physics}

\subsection{Continuity of the PhD}
\emph{Collaborators: Denis Lacroix, Marcella Grasso.}


\subsubsection{Pairing and resummation}
The first step is to make the same semi-analitical calculation with simplified interaction (simple contact for instance) as done by Kaiser by resumming ladder contributions for the energy/self-energy.
However, even at second order, by replacing the $2p-2h$ energy by the $4$ quasi-particles, the complexity of the integrals to tackle increases significantly.


\subsubsection{$\omega$-dependence of the resummed self-energy}
An extension to include off-shell effects would be \apriori desirable especially to describe the $E$-mass (see works of Mahaux \etal).

\subsubsection{Applications to finite systems (quantum drop) with density-dependent Skyrme-like functional}
Simple and basic use of HF code for quantum drop using the functionals developed during my PhD as a proof of principle.



\subsection{MBPT extended with constraints \label{sec:MBPTconstrained}}
\emph{Collaborator: Denis Lacroix.}

The aim consist to be able to perform \mbpt for dilute systems, \eg ultracold atoms, where at HF level the first order at low density and the Bertsch parameter are well reproduced. This require to use another contact interaction containing explicitly the correct value of the Bertsch parameter.
Then, at second order, the use of modified contact interaction is required.
Basically, the idea consist to find a systematic way to construct the contact interaction at $n$th order (containing physical insight) giving the required limit at the $n$th order in \mbpt for dilute systems and the physical limits present in this $n$th order interaction.
For more detail, see the notes at \url{https://gitlab.com/antoineboulet/working-notes} \texttt{[MBPT-constrained]}: \href{../MBPT-constrained/main.pdf}{\emph{MBPT constrained and BMF DFT: a step towards the EFT formalism of EDF}}.


\section{Possible collaboration with QMC community}
\emph{Collaborators: Alex Gezerlis, Denis Lacroix.}

We would like find a general form of functional for infinite matter (based on resummation for instance) where the parameters are adjusted on \abi results (effective mass, \etc).
One of the possible way is to be inspired by the \slda of Bulgac \etal for unitary systems. Another possibility could be based on a parametrization in term of the density \emph{(i)} of the Landau parameter, \emph{(ii)} Skyrme-like functional with density-dependent coupling constants.
A way to do this could be the use of \mbpt extended with constraints discussed in \ref{sec:MBPTconstrained}.


\section{\emph{Ab initio} calculations for semi-infinite matter}
\emph{Collaborator: Denis Lacroix.}

The idea is to fix some problem and find some hints concerning the difficulties to reproduce properly the radius/shape of density profiles using \abi method and chiral interactions (this issue actually strongly depend of the chiral potential used).

\section{Symmetry broken and restored self-consistent Green's function theory}
\emph{Collaborators: Thomas Duguet, Vittorio Somà.}

\href{../../../research_project_PGoSCGF/research_project_PGoSCGF.pdf}{\texttt{Research project.}}






\input{BackEndMatter.tex} \end{document}
