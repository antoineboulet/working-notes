\contentsline {title}{Generalities on the density matrix expansion}{1}{section*.2}
\contentsline {abstract}{Abstract}{1}{section*.1}
\tocdepth@munge 
\contentsline {section}{\numberline {}Contents}{1}{section*.3}
\tocdepth@restore 
\contentsline {section}{\numberline {I}Mean-field description of many-body quantum systems}{1}{section*.4}
\contentsline {subsection}{\numberline {A}Hartree-Fock theory}{1}{section*.5}
\contentsline {subsubsection}{\numberline {1}Complete quantum basis}{1}{section*.6}
\contentsline {subsubsection}{\numberline {2}Many-body state of quantum systems}{2}{section*.7}
\contentsline {subsubsection}{\numberline {3}Hartree-Fock equations}{2}{section*.8}
\contentsline {subsection}{\numberline {B}Hartree-Fock-Bogoliubov theory}{2}{section*.9}
\contentsline {subsubsection}{\numberline {1}Bogoliubov transformation}{2}{section*.10}
\contentsline {subsubsection}{\numberline {2}Quasi-particle density}{3}{section*.11}
\contentsline {subsubsection}{\numberline {3}Hartree-Fock-Bogoliubov equations}{3}{section*.12}
\contentsline {subsection}{\numberline {C}One-body density matrix in coordinate space}{3}{section*.13}
\contentsline {section}{\numberline {II}Hartree-Fock energy with a general two-body Hamiltonian}{3}{section*.14}
\contentsline {subsection}{\numberline {A}Kinetic contribution to the Hartree-Fock energy}{4}{section*.15}
\contentsline {subsection}{\numberline {B}Hartree-Fock energy of the two-body interaction}{4}{section*.16}
\contentsline {subsection}{\numberline {C}Decomposition of the two-body interaction}{5}{section*.17}
\contentsline {subsection}{\numberline {D}Hartree contribution of the two-body local interaction}{5}{section*.18}
\contentsline {subsubsection}{\numberline {1}Central part of the Hartree contribution}{6}{section*.19}
\contentsline {subsubsection}{\numberline {2}Tensor part of the Hartree contribution}{6}{section*.20}
\contentsline {subsubsection}{\numberline {3}Spin-orbit part of the Hartree contribution}{6}{section*.21}
\contentsline {subsection}{\numberline {E}Fock contribution of the two-body local interaction}{6}{section*.22}
\contentsline {subsubsection}{\numberline {1}Central part of the Fock contribution}{6}{section*.23}
\contentsline {subsubsection}{\numberline {2}Tensor part of the Fock contribution}{6}{section*.24}
\contentsline {subsubsection}{\numberline {3}Spin-orbit part of the Fock contribution}{7}{section*.25}
\contentsline {subsection}{\numberline {F}Summary of the two-body local interaction contribution to the Hartree-Fock energy}{7}{section*.26}
\contentsline {subsection}{\numberline {G}Exact Hartree-Fock equation for an idealized system}{8}{section*.27}
\contentsline {section}{\numberline {III}Application of the density-matrix expansion for the two-body interaction}{9}{section*.28}
\contentsline {subsection}{\numberline {A}Phase-space average method for the density-matrix expansion}{9}{section*.29}
\contentsline {subsubsection}{\numberline {1}Isolation of the non-locality of the one-body density matrix}{9}{section*.30}
\contentsline {subsubsection}{\numberline {2}Expansion around a momentum scale }{10}{section*.31}
\contentsline {subsubsection}{\numberline {3}Phase-space average method}{10}{section*.32}
\contentsline {subsubsection}{\numberline {4}Calculation of the $\Pi _n$ functions}{10}{section*.33}
\contentsline {subsubsection}{\numberline {5}Choice of averaged momentum scale}{11}{section*.34}
\appendix 
\contentsline {section}{\numberline {A}Spin-isospin decomposition of the one-body density matrix in coordinate space }{12}{section*.35}
\contentsline {subsection}{\numberline {1}Spin-isospin independent one-body density matrix}{12}{section*.36}
\contentsline {subsection}{\numberline {2}Isospin independent one-body density matrix}{12}{section*.37}
\contentsline {subsubsection}{\numberline {a}General case}{12}{section*.38}
\contentsline {subsubsection}{\numberline {b}Spherical representation of the Pauli matrices}{12}{section*.39}
\contentsline {subsubsection}{\numberline {c}Pure spin states}{13}{section*.40}
\contentsline {subsection}{\numberline {3}One-body density matrix with spin and isospin}{13}{section*.41}
\contentsline {subsubsection}{\numberline {a}General case}{13}{section*.42}
\contentsline {subsubsection}{\numberline {b}Pure spin states}{13}{section*.43}
\contentsline {subsubsection}{\numberline {c}Pure isospin states}{13}{section*.44}
\contentsline {subsubsection}{\numberline {d}Pure spin-isospin states}{14}{section*.45}
\contentsline {section}{\numberline {B}Hartree-Fock energy for infinite matter}{14}{section*.46}
\contentsline {section}{\numberline {}References}{15}{section*.47}
